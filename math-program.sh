#Kehli Cousins 1602962
#!/bin/sh

NUM1=$1
NUM2=$2
NUM3=$3

if [ -z ${OUTPUT_FILE_NAME} ]
then
	OUTPUT_FILE_NAME="build/myresult.txt"	
	echo "Default result filename '$OUTPUT_FILE_NAME' is in use."
else
	
	echo "Given file name in use is '$OUTPUT_FILE_NAME'."
fi

echo "Calculating : ($NUM1 ^ $NUM2)+ $NUM3 x $NUM2"

RESULT=$((NUM1**NUM2 +NUM3 * NUM2 ))

RESULT_OUTPUT="RESULT calculated from ($NUM1 ^ $NUM2)+ $NUM3 x $NUM2 = $RESULT"

echo $RESULT_OUTPUT


echo $RESULT_OUTPUT >> $OUTPUT_FILE_NAME 
